# Humiduino
This is an Arduino app that monitors soil humidity and beeps when it's above a threshold.

## Components
- Humidity sensor
- Buzzer
- 16x2 LCD display