#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
const byte boardLED = 13;
const byte buzzerPin = 7;

int humidityValue = 50;
int humidityThreshold = 60;
const unsigned long humidityInterval = 5000;
const unsigned long buzzerInterval = 1000;

unsigned long humidityTimer = 0;
unsigned long buzzerTimer = 0;

int buzzerState = LOW;
int buzzerFreq = 80;

void setup() {
  lcd.begin(16, 2);

  pinMode(boardLED, OUTPUT);
  pinMode(buzzerPin, OUTPUT);

  buzzerTimer = buzzerInterval;
  humidityTimer = humidityInterval;
}

void enableHumidityThresholdAlarm() {
  if ((millis() - buzzerTimer) >= buzzerInterval) {

    // stop beeping below humidityThreshold
    if (humidityValue <= humidityThreshold) {
      noTone(buzzerPin);
      buzzerState = LOW;
      digitalWrite(boardLED, LOW);
    } else {
      digitalWrite(boardLED, HIGH);
      if (buzzerState == LOW) {
        buzzerState = HIGH;
        // the third parameter has the effect
        // of multiplying the buzzerInterval by 2
        tone(buzzerPin, buzzerFreq, 40);
      } else {
        buzzerState = LOW;
        noTone(buzzerPin);
      }
      digitalWrite(buzzerPin, buzzerState);
      buzzerTimer = millis();
    }
  }
}

void enableHumidityMonitor() {
  if ((millis() - humidityTimer) >= humidityInterval) {

    // mock sensor reading
    humidityValue = humidityValue + random(-2, 3);
    if (humidityValue > 99 || humidityValue < 0) {
      humidityValue = 50;
    }

    // show on screen
    lcd.setCursor(0, 0);
    lcd.clear();
    lcd.print("Umid: ");
    lcd.print(humidityValue);

    humidityTimer = millis();
  }
}

void loop() {
  enableHumidityMonitor();
  enableHumidityThresholdAlarm();
}
